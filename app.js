const inquirer = require('inquirer');
const chalk = require('chalk');
const checkAnagram = require('./anagrams.js');

/**
 * Builds a message saying whether or not a phrase is an anagram for another phrase.
 * @param {string} subject - Phrase subject of an anagram
 * @param {string} anagram - Anagram for the subject phrase
 * @param {boolean} isAnagram - If the `anagram` is an angram for `subject`
 * @returns {string} Message saying whether or not `anagram` is an anagram for `subject`
 */
function anagramMsg(subject, anagram, isAnagram) {
	return `${isAnagram ? chalk.green('\u2713') : chalk.red('\u274C')}\
'${subject}' ${isAnagram ? 'is' : 'is not'} an anagram for '${anagram}'`;
}

/**
 * Validates de user input, only accepting letters and spaces.
 * @param {string} input - user input
 * @returns {any} - true if valid, otherwise a custom message is shown.
 */
function validateInput(input) {
	var valid = input.match(/^[a-zA-Z\s]+$/);
	return valid ? true : 'Phrase must contain only letters and spaces.';
}

/**
 * Questions for the user input.
 */
var questions = [
	{
		type: 'input',
		name: 'subject',
		message: 'Phrase subject to anagram:',
		validate: validateInput
	},
	{
		type: 'input',
		name: 'anagram',
		message: 'Anagram for subject phrase:',
		validate: validateInput
	}
];

/**
 * Prompt for the user input of the subject and anagram.
 */
inquirer.prompt(questions).then(function (answers) {
	var subject = answers.subject;
	var anagram = answers.anagram;

	var	isAnagram = checkAnagram(subject, anagram);
	console.log(anagramMsg(subject, anagram, isAnagram));
});
