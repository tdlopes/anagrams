Setup
-----

Requires Node.js and NPM to run.

```sh
npm install
```

Run
---
```sh
npm start
```

Test
----

```sh
npm test
```
