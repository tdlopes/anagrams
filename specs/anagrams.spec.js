const checkAnagram = require('../anagrams.js'); // Anagram library to be tested
const assert = require('chai').assert // Assert library

describe('Anagrams', function () {
	it('should correctly check an anagram', function() {
		assert.isOk(checkAnagram('William Shakespeare', 'I am a weakish speller'));
		assert.isOk(checkAnagram('Tom Marvolo Riddle', 'I am Lord Voldemort'));
	});
	it('should correctly check a non anagram', function() {
		assert.isNotOk(checkAnagram('Abc', 'Bcd'));
		assert.isNotOk(checkAnagram('hello?','hello'));
	});
});
