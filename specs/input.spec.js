const assert = require('chai').assert // Assert library
const run = require('inquirer-test'); // Inquirer runner
const {ENTER} = require('inquirer-test'); // Enter key unicode.
const cliPath = __dirname + '/../app.js'; // CLI Application

describe('prompt', function() {
	it('should not accept empty input', async function() {
		var output = await(run(cliPath, [ENTER]));
		// Slice of the expected output as a regular expression
		var expected = new RegExp('must contain only letters and spaces', 'g');
		assert.isOk(expected.test(output));
	});

	it('should not accept characters besides letters and spaces', async function(){
		var output = await(run(cliPath, ['hello there?', ENTER]));
		// Slice of the expected output as a regular expression
		var expected = new RegExp('must contain only letters and spaces', 'g');
		assert.isOk(expected.test(output));

		var output = await(run(cliPath, ['hello', ENTER, 'ellh?', ENTER]));
		// Slice of the expected output as a regular expression
		var expected = new RegExp('must contain only letters and spaces', 'g');
		assert.isOk(expected.test(output));
	});

	it('should accept user input and check for anagrams', async function() {
		var output = await(run(cliPath, ['hello', ENTER, 'ellho', ENTER]));
		// Slice of the expected output as a regular expression
		var expected = new RegExp('is an anagram', 'g');
		assert.isOk(expected.test(output));
	});
});
