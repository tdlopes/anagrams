/**
 * Checks if a given phrase is an anagram for another phrase.
 * Both strings are expected to contain only letters and spaces and be in lowercase.
 * Each string is split into an array of letters and sorted in alphabetical order.
 * Due to the nature of an anagram, the resulting sorted strings should be identical.
 *
 * @param {string} subject - Phrase subject of an anagram
 * @param {string} anagram - Anagram for the subject phrase
 * @returns {boolean} - true if `anagram` is an anagram for `subject`
 */
module.exports = (subject, anagram) => {
	// Remove the spaces from both the subject and the anagram
  var subjectNoSpaces = subject.toLowerCase().replace(/\s+/g, '');
	var anagramNoSpaces = anagram.toLowerCase().replace(/\s+/g, '');

	if (subjectNoSpaces.length != anagramNoSpaces.length) {
		return false; // Check length first before checking further
	} else {
		// Sort the letters from both the subject and the anagram in alphabetical order
		var subjectSortedLetters = subjectNoSpaces.split('').sort().join('');
		var anagramSortedLetters = anagramNoSpaces.split('').sort().join('');

		return subjectSortedLetters == anagramSortedLetters;
	}
}	
